#define BENCHMARK "OSU Message Rate Test"

#include "osu_1sc.h"

#define SKIP_LARGE  50
#define LOOP_LARGE  300

#define LARGE_MESSAGE_SIZE 8192
#define LARGE_OPS_NUM 10000

#define MYBUFSIZE 100000000

int skip = 10, loop = 100;

double t_start = 0.0, t_end = 0.0, t_duration = 0.0, t_duration_avg = 0.0;

char obuf_original[MYBUFSIZE], tbuf_original[MYBUFSIZE];
char rbuf_original[MYBUFSIZE], cbuf_original[MYBUFSIZE];
char *obuf = NULL, *tbuf = NULL, *rbuf = NULL, *cbuf = NULL, *wbuf = NULL;

void print_header(WINDOW, SYNC, OP, TYPE);
void print_mr(int, int, int, double);
void run_op(int, int, WINDOW, OP, SYNC, TYPE, int, int);

int main (int argc, char *argv[]) {
    int rank, nprocs;
    int po_ret = po_okay;

    WINDOW win_type = WIN_ALLOCATE;
    SYNC sync_type = FLUSH;
    OP op_type = PUT;
    TYPE type = ATA;
    int ops_num = 1;
    int data_size = 1;

    /* parse input arguments */
    po_ret = process_options(argc, argv,
                             &win_type, &sync_type, &op_type, &type,
                             &ops_num, &data_size);

    MPI_CHECK(MPI_Init(&argc, &argv));
    MPI_CHECK(MPI_Comm_size(MPI_COMM_WORLD, &nprocs));
    MPI_CHECK(MPI_Comm_rank(MPI_COMM_WORLD, &rank));

    if (rank == 0) {
        switch (po_ret) {
        case po_bad_usage:
        case po_help_message:
            usage();
            break;
        }
    }

    switch (po_ret) {
    case po_bad_usage:
        MPI_Finalize();
        exit(EXIT_FAILURE);
    case po_help_message:
        MPI_Finalize();
        exit(EXIT_SUCCESS);
    case po_okay:
        break;
    }

    if (rank == 0) {
        print_header(win_type, sync_type, op_type, type);
    }

    run_op(rank, nprocs,
           win_type, op_type, sync_type, type,
           ops_num, data_size);

    MPI_CHECK(MPI_Finalize());

    return EXIT_SUCCESS;
}

void print_header (WINDOW win, SYNC sync, OP op, TYPE type) {
    fprintf(stdout, "# Window creation: %s\n",
            win_info[win]);
    fprintf(stdout, "# Synchronization: %s\n",
            sync_info[sync]);
    fprintf(stdout, "# Operation: %s\n",
            op_info[op]);
    fprintf(stdout, "# Test type: %s\n",
            type_info[type]);

    /* Print order (left to right):
     * message size, numbe of ops, number of processes, message rate (to each target) */
    fprintf(stdout, "%-*s%-*s%-*s%-*s%-*s\n", 10, "# Size", 10, "#Ops", 10, "#Procs",
            25, "Message Rate (#Msg/s)", 25, "Execution time (us)");

    fflush(stdout);
}

void print_mr(int size, int ops_num, int procs_num, double t) {

    /* Print order (left to right):
     * message size, numbe of ops, number of processes, message rate */
    fprintf(stdout, "%-*d%-*d%-*d%-*.*f%-*.*f\n", 10, size, 10, ops_num, 10, procs_num,
            25, FLOAT_PRECISION, (double)ops_num / t,
            25, FLOAT_PRECISION, (double)(t*1000000));

    fflush(stdout);
}

void run_op(int rank, int nprocs,
            WINDOW win_type, OP op_type, SYNC sync_type, TYPE type,
            int ops_num, int data_size) {

    int i, j, k, target;
    MPI_Win win;
    MPI_Group comm_group, group, root_group, non_root_group;
    int *destrank;

    allocate_memory(obuf_original, tbuf_original, rbuf_original, cbuf_original,
                    MYBUFSIZE, win_type, MPI_COMM_WORLD,
                    &obuf, &tbuf, &rbuf, &cbuf, &wbuf, &win);

    if(data_size > LARGE_MESSAGE_SIZE ||
       ops_num > LARGE_OPS_NUM) {
        loop = LOOP_LARGE;
        skip = SKIP_LARGE;
    }

    if (sync_type == PSCW) {
        int zero = 0;

        MPI_Comm_group(MPI_COMM_WORLD, &comm_group);

        destrank = malloc(sizeof(int)*nprocs);
        for (i = 0; i < nprocs; i++)
            destrank[i] = i;

        MPI_CHECK(MPI_Group_incl(comm_group, nprocs, destrank, &group));
        free(destrank);

        MPI_CHECK(MPI_Group_incl(comm_group, 1, &zero, &root_group));
        MPI_CHECK(MPI_Group_excl(comm_group, 1, &zero, &non_root_group));
    }

    MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));

    if (sync_type == FLUSH_LOCAL || sync_type == FLUSH) {
        MPI_CHECK(MPI_Win_lock_all(0, win));
    }

    for (i = 0; i < skip + loop; i++) {
        if (i == skip) {
            t_start = MPI_Wtime();
        }

        if (sync_type == FENCE) {
            MPI_CHECK(MPI_Win_fence(MPI_MODE_NOPRECEDE, win));
        }
        else if (sync_type == PSCW) {
            if (type == ATA) {
                MPI_CHECK(MPI_Win_post(group, 0, win));
                MPI_CHECK(MPI_Win_start(group, 0, win));
            }
            else {
                if (rank != 0) {
                    MPI_CHECK(MPI_Win_post(root_group, 0, win));
                }
                if (rank == 0) {
                    MPI_CHECK(MPI_Win_start(non_root_group, 0, win));
                }
            }
        }
        else if (sync_type == LOCK_ALL) {
            MPI_CHECK(MPI_Win_lock_all(0, win));
        }

        if ((type == OTA && rank == 0) ||  /* One-to-All */
            (type == ATA)) {               /* All-to-All */

            for (k = rank + 1; k < rank + nprocs; k++) {

                target = k % nprocs;

                if (sync_type == LOCK) {
                    MPI_CHECK(MPI_Win_lock(MPI_LOCK_SHARED, target, 0, win));
                }

                for(j = 0; j < ops_num; j++) {
                    switch(op_type) {
                    case PUT:
                        MPI_CHECK(MPI_Put(obuf, data_size, MPI_CHAR,
                                          target, 0, data_size, MPI_CHAR, win));
                        break;
                    case GET:
                        MPI_CHECK(MPI_Get(obuf, data_size, MPI_CHAR,
                                          target, 0, data_size, MPI_CHAR, win));
                        break;
                    case ACC:
                        MPI_CHECK(MPI_Accumulate(obuf, data_size, MPI_CHAR,
                                                 target, 0, data_size, MPI_CHAR, MPI_SUM, win));
                        break;
                    case GACC:
                        MPI_CHECK(MPI_Get_accumulate(obuf, data_size, MPI_CHAR,
                                                     rbuf, data_size, MPI_CHAR,
                                                     target, 0, data_size, MPI_CHAR, MPI_SUM, win));
                        break;
                    case FOP:
                        MPI_CHECK(MPI_Fetch_and_op(obuf, rbuf, MPI_CHAR,
                                                   target, 0, MPI_SUM, win));
                        break;
                    case CAS:
                        MPI_CHECK(MPI_Compare_and_swap(obuf, cbuf, rbuf, MPI_CHAR,
                                                       target, 0, win));
                        break;
                    default:
                        break;
                    }
                } /* end of loop for #ops */

                if (sync_type == FLUSH_LOCAL) {
                    MPI_CHECK(MPI_Win_flush_local(target, win));
                }
                else if (sync_type == FLUSH) {
                    MPI_CHECK(MPI_Win_flush(target, win));
                }
                else if (sync_type == LOCK) {
                    MPI_CHECK(MPI_Win_unlock(target, win));
                }
            } /* end of loop for #procs */
        }

        if (sync_type == FENCE) {
            MPI_CHECK(MPI_Win_fence(MPI_MODE_NOSUCCEED, win));
        }
        else if (sync_type == PSCW) {
            if (type == ATA) {
                MPI_CHECK(MPI_Win_complete(win));
                MPI_CHECK(MPI_Win_wait(win));
            }
            else {
                if (rank == 0) {
                    MPI_CHECK(MPI_Win_complete(win));
                }
                else {
                    MPI_CHECK(MPI_Win_wait(win));
                }
            }
        }
        else if (sync_type == LOCK_ALL) {
            MPI_CHECK(MPI_Win_unlock_all(win));
        }
    } /* end of loop for tests */

    t_end = MPI_Wtime();

    if (sync_type == FLUSH || sync_type == FLUSH_LOCAL) {
        MPI_CHECK(MPI_Win_unlock_all(win));
    }

    t_duration = (t_end - t_start) / loop;

    if (type == ATA) {
        MPI_Reduce(&t_duration, &t_duration_avg, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
        if (rank == 0) {
            t_duration_avg /= nprocs;
            t_duration_avg /= (nprocs - 1); /* always exclude myself */
            print_mr(data_size, ops_num, nprocs, t_duration_avg);
        }
    }
    else {
        if (rank == 0) {
            t_duration_avg = t_duration;
            t_duration_avg /= (nprocs - 1); /* always exclude myself */
            print_mr(data_size, ops_num, nprocs, t_duration_avg);
        }
    }

    if (sync_type == PSCW) {
        MPI_CHECK(MPI_Group_free(&group));
        MPI_CHECK(MPI_Group_free(&comm_group));
        MPI_CHECK(MPI_Group_free(&root_group));
        MPI_CHECK(MPI_Group_free(&non_root_group));
    }

    free_memory(&win);
}
