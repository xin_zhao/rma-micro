#include "osu_1sc.h"

char *win_info[20] = {
    "MPI_Win_create",
    "MPI_Win_allocate",
};

char *sync_info[20] = {
    "MPI_Win_lock/unlock",
    "MPI_Win_post/start/complete/wait",
    "MPI_Win_fence",
    "MPI_Win_flush",
    "MPI_Win_flush_local",
    "MPI_Win_lock_all/unlock_all",
};

char *op_info[20] = {
    "MPI_Put",
    "MPI_Get",
    "MPI_Accumulate",
    "MPI_Get_accumulate",
    "MPI_Fetch_and_op",
    "MPI_Compare_and_swap",
};

char *type_info[20] = {
    "All-to-All (exclude sender itself)",
    "One-to-All (exclude sender itself)",
};

void usage() {
    printf("Usage: ./benchmark [options] \n");

    printf("options:\n");

    printf("\n");

    printf("  -w <win_option>\n");
    printf("            <win_option> can be any of the follows:\n");
    printf("            create            use MPI_Win_create to create an MPI Window object\n");
    printf("            allocate          use MPI_Win_allocate to create an MPI Window object\n");
    printf("\n");

    printf("  -s <sync_option>\n");
    printf("            <sync_option> can be any of the follows:\n");
    printf("            pscw              use Post/Start/Complete/Wait synchronization calls\n");
    printf("            fence             use MPI_Win_fence synchronization call\n");
    printf("            lock              use MPI_Win_lock/unlock synchronizations calls\n");
    printf("            flush             use MPI_Win_flush synchronization call\n");
    printf("            flush_local       use MPI_Win_flush_local synchronization call\n");
    printf("            lock_all          use MPI_Win_lock_all/unlock_all synchronization calls\n");
    printf("\n");

    printf("  -o <op_option>\n");
    printf("            <op_option> can be any of the follows:\n");
    printf("            put             use Put communication call\n");
    printf("            get             use Get communication call\n");
    printf("            acc             use Accumulate communication call\n");
    printf("            gacc            use Get_accumulate communication call\n");
    printf("            fop             use Fetch_and_op communication call\n");
    printf("            cas             use Compare_and_swap communication call\n");
    printf("\n");

    printf("  -t <type_option>\n");
    printf("            <type_option> can be any of the follows:\n");
    printf("            ata             All-to-All\n");
    printf("            ota             One-to-All\n");
    printf("\n");

    printf("  -n <ops_num>\n");
    printf("\n");

    printf("  -m <data_size>\n");
    printf("\n");

    printf("  -h print this help message\n");
    printf("\n");

    fflush(stdout);
}

int process_options(int argc, char *argv[],
                    WINDOW *win, SYNC *sync, OP *op, TYPE *type,
                    int *ops_num, int *data_size) {

    int c;
    extern char *optarg;
    char const * optstring = "+w:s:o:t:n:m:h";

    while((c = getopt(argc, argv, optstring)) != -1) {
        switch (c) {
        case 'w':
            if (0 == strcasecmp(optarg, "create")) {
                *win = WIN_CREATE;
            }
            else if (0 == strcasecmp(optarg, "allocate")) {
                *win = WIN_ALLOCATE;
            }
            else {
                return po_bad_usage;
            }
            break;
        case 's':
            if (0 == strcasecmp(optarg, "pscw")) {
                *sync = PSCW;
            }
            else if (0 == strcasecmp(optarg, "fence")) {
                *sync = FENCE;
            } 
            else if (0 == strcasecmp(optarg, "lock")) {
                *sync = LOCK;
            }
            else if (0 == strcasecmp(optarg, "flush")) {
                *sync = FLUSH;
            }
            else if (0 == strcasecmp(optarg, "flush_local")) {
                *sync = FLUSH_LOCAL;
            }
            else if (0 == strcasecmp(optarg, "lock_all")) {
                *sync = LOCK_ALL;
            }
            else { 
                return po_bad_usage;
            }
            break;
        case 'o':
            if (0 == strcasecmp(optarg, "put")) {
                *op = PUT;
            }
            else if (0 == strcasecmp(optarg, "get")) {
                *op = GET;
            }
            else if (0 == strcasecmp(optarg, "acc")) {
                *op = ACC;
            }
            else if (0 == strcasecmp(optarg, "gacc")) {
                *op = GACC;
            }
            else if (0 == strcasecmp(optarg, "fop")) {
                *op = FOP;
            }
            else if (0 == strcasecmp(optarg, "cas")) {
                *op = CAS;
            }
            else {
                return po_bad_usage;
            }
            break;
        case 't':
            if (0 == strcasecmp(optarg, "ata")) {
                *type = ATA;
            }
            else if (0 == strcasecmp(optarg, "ota")) {
                *type = OTA;
            }
            else {
                return po_bad_usage;
            }
            break;

        case 'n':
            *ops_num = atoi(optarg);
            break;
        case 'm':
            *data_size = atoi(optarg);
            break;
        case 'h':
            return po_help_message;
        default:
            return po_bad_usage;
        }
    }
    return po_okay;
}

void *align_buffer(char *ptr, int align_size) {
    return ((char *)ptr + (align_size - 1) / align_size * align_size);
}

/* Input: obuf_orig, tbuf_orig, rbuf_orig, cbuf_orig, size, type, comm
 * Output: obuf, ybuf, rbuf, cbuf, win_base, win */
void allocate_memory(char *obuf_orig, char *tbuf_orig, char *rbuf_orig, char *cbuf_orig,
                     int size, WINDOW type, MPI_Comm comm,
                     char **obuf, char **tbuf, char **rbuf, char **cbuf,
                     char **win_base, MPI_Win *win) {

    int page_size = getpagesize();
    assert(page_size <= MAX_ALIGNMENT);

    *obuf = align_buffer(obuf_orig, page_size);  /* origin buffer */
    memset(*obuf, 'a', size);
    *tbuf = align_buffer(tbuf_orig, page_size);  /* target buffer */
    memset(*tbuf, 'b', size);
    *rbuf = align_buffer(rbuf_orig, page_size);  /* result buffer */
    memset(*rbuf, 'c', size);
    *cbuf = align_buffer(cbuf_orig, page_size);  /* compare buffer */
    memset(*cbuf, 'd', size);

    switch (type) {
    case WIN_CREATE:
        MPI_CHECK(MPI_Win_create(*tbuf, size, sizeof(char), MPI_INFO_NULL, comm, win));
        (*win_base) = *tbuf;
        break;
    default:
        assert(type == WIN_ALLOCATE);
        MPI_CHECK(MPI_Win_allocate(size, sizeof(char), MPI_INFO_NULL, comm, win_base, win));
        break;
    }
}

void free_memory(MPI_Win *win) {
    MPI_Win_free(win);
}
