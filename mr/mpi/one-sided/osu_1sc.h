#include <mpi.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <getopt.h>

#define MAX_ALIGNMENT 65536

#ifndef FIELD_WIDTH
#   define FIELD_WIDTH 20
#endif

#ifndef FLOAT_PRECISION
#   define FLOAT_PRECISION 2
#endif

#define CHECK(stmt)                                              \
do {                                                             \
   int errno = (stmt);                                           \
   if (0 != errno) {                                             \
       fprintf(stderr, "[%s:%d] function call failed with %d \n",\
        __FILE__, __LINE__, errno);                              \
       exit(EXIT_FAILURE);                                       \
   }                                                             \
   assert(0 == errno);                                           \
} while (0)

#define MPI_CHECK(stmt)                                          \
do {                                                             \
   int mpi_errno = (stmt);                                       \
   if (MPI_SUCCESS != mpi_errno) {                               \
       fprintf(stderr, "[%s:%d] MPI call failed with %d \n",     \
        __FILE__, __LINE__,mpi_errno);                           \
       exit(EXIT_FAILURE);                                       \
   }                                                             \
   assert(MPI_SUCCESS == mpi_errno);                             \
} while (0)

/*structures, enumerators and such*/
/* Window creation */
typedef enum {
    WIN_CREATE=0,
    WIN_ALLOCATE,
} WINDOW;

/* Synchronization */
typedef enum {
    LOCK=0,
    PSCW,
    FENCE,
    FLUSH,
    FLUSH_LOCAL,
    LOCK_ALL,
} SYNC;

typedef enum {
    PUT=0,
    GET,
    ACC,
    GACC,
    FOP,
    CAS,
} OP;

typedef enum {
    ATA=0,
    OTA,
} TYPE;

enum po_ret_type {
    po_bad_usage,
    po_help_message,
    po_okay,
};

/*variables*/
extern char *win_info[20];
extern char *sync_info[20];
extern char *op_info[20];
extern char *type_info[20];

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

/*function declarations*/
void usage();
int  process_options(int, char**, WINDOW*, SYNC*, OP*, TYPE*, int*, int*);
void allocate_memory(char*, char*, char*, char*,
                     int, WINDOW, MPI_Comm,
                     char**, char**, char**, char**,
                     char**, MPI_Win*);
void free_memory(MPI_Win*);
