#!/bin/bash

if [ $# -lt 4 ];then
    echo "./filter_osu.sh <file> <keymin> <keymax> <keyiter>"
    exit
fi

file=$1
keymin=$2
keymax=$3
keyiter=$4

if [ ! -f $file ];then
    echo "File $f not exist."
    exit
fi

echo "<$file> <$keymin> <$keymax> <$keyiter>"

## filter out the 4th column
k=$keymin
while [ $k -le $keymax ]; do
    cat $file | grep "^$k " | awk '{printf("%s\t", $4)}'
    echo $k | awk '{{printf"%s\t", $4}}'
    echo ""
    if [ $k -eq 0 ]; then
        let k=1;
    else
        let k*=$keyiter
    fi
done
