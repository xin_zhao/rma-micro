#!/bin/sh

## submit all jobs of create_inter
for i in ./run_create_inter_* ; do
    output1="$(grep "np=" ${i})"
    output2="$(tr -cd 0-9 <<<"$output1")"

    nodes=$output2    
    walltime=02:00:00

    command="qsub -l nodes=${nodes}:ppn=8 -l walltime=${walltime} -j oe ${i}"
    echo $command
    $command
done

## submit all jobs of create_intra
for i in ./run_create_intra_* ; do
    output1="$(grep "np=" ${i})"
    output2="$(tr -cd 0-9 <<<"$output1")"

    nodes="$(expr ${output2} / 8)"
    if [[ ${nodes} == "0" ]] ; then
        nodes=1
    fi
    walltime=02:00:00

    command="qsub -l nodes=${nodes}:ppn=8 -l walltime=${walltime} -j oe ${i}"
    echo $command
    $command
done

## submit all jobs of allocate_intra
for i in ./run_allocate_intra_* ; do
    output1="$(grep "np=" ${i})"
    output2="$(tr -cd 0-9 <<<"$output1")"

    nodes="$(expr ${output2} / 8)"
    if [[ ${nodes} == "0" ]] ; then
        nodes=1
    fi
    walltime=02:00:00

    command="qsub -l nodes=${nodes}:ppn=8 -l walltime=${walltime} -j oe ${i}"
    echo $command
    $command
done

