#!/bin/sh

op=
sync=
nops=
np=

ppn=8
if [[ ${np} < 8 ]]; then
    ppn=${np}
fi

## total number of loops
loop_num=20

## message size
min_msg_size=1
max_msg_size=65536
msg_size_interval=4

cd $PBS_O_WORKDIR

## results path
results_path=/homes/xinzhao/projects/eval/results/results-micro

## application path
osu_bench_rsch_install_path=/homes/xinzhao/projects/eval/rma-benchmarks-research/osu-micro-benchmarks-4.4.1/install/libexec/osu-micro-benchmarks/mpi/one-sided
osu_bench_rls_install_path=/homes/xinzhao/projects/eval/rma-benchmarks-release/osu-micro-benchmarks-4.4.1/install/libexec/osu-micro-benchmarks/mpi/one-sided

## mpich path
mpich_rsch_install_path=/homes/xinzhao/projects/impl/mpich/research/builds/build-device-mxm-strict/install/bin
mpich_rls_install_path=/homes/xinzhao/projects/impl/mpich/release/builds/build-device-mxm-strict/install/bin

## set mpich CVAR values
export MPIR_CVAR_CH3_RMA_NREQUEST_THRESHOLD=128
export MPIR_CVAR_CH3_RMA_NREQUEST_NEW_THRESHOLD=0
export MPIR_CVAR_CH3_RMA_OP_GLOBAL_POOL_SIZE=300000
export MPIR_CVAR_CH3_RMA_OP_EAGER_ISSUING_INTERVAL=100

## timeout for each program is 5 min (300 seconds)
export MPIEXEC_TIMEOUT=300

## testing
for ((j=$min_msg_size;j<=$max_msg_size;j*=$msg_size_interval))
do
  for ((i=0;i<$loop_num;i++))
  do
      ## test for inter-node communication
      $mpich_rls_install_path/mpiexec -n ${np} -ppn ${ppn} -bind-to rr $osu_bench_rls_install_path/osu_mr -o ${op} -w create -s ${sync} -n ${nops} -m ${j} >> ${results_path}/RLS_result_${op}_${sync}_o${nops}_p${np}_create_intra
      $mpich_rsch_install_path/mpiexec -n ${np} -ppn ${ppn} -bind-to rr $osu_bench_rsch_install_path/osu_mr -o ${op} -w create -s ${sync} -n ${nops} -m ${j} >> ${results_path}/RSCH_result_${op}_${sync}_o${nops}_p${np}_create_intra
  done
done
