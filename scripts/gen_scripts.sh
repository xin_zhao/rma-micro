#!/bin/sh

## set np
min_np=2
max_np=128
interval_np=4

## set nops
min_nops=1
max_nops=4096
interval_nops=16

## loop over test types
for ((k=0; k<3; k++))
do
  if [[ $k == 0 ]] ; then
      test_type=create_inter
  fi
  if [[ $k == 1 ]] ; then
      test_type=create_intra
  fi
  if [[ $k == 2 ]] ; then
      test_type=allocate_intra
  fi

  ## loop over op types
  for ((i=0; i<4; i++))
  do
    if [[ $i == 0 ]] ; then
        op=put
    fi
    if [[ $i == 1 ]] ; then
        op=get
    fi
    if [[ $i == 2 ]] ; then
        op=acc
    fi
    if [[ $i == 3 ]] ; then
        op=gacc
    fi

    cp ./basic_${test_type}.sh ./basic_${test_type}_${op}.sh
    sed -i "s/op=/op=${op}/g" ./basic_${test_type}_${op}.sh

    ## loop over sync types
    for ((j=0; j<6; j++))
    do
      if [[ $j == 0 ]] ; then
          sync=fence
      fi
      if [[ $j == 1 ]] ; then
          sync=pscw
      fi
      if [[ $j == 2 ]] ; then
          sync=lock
      fi
      if [[ $j == 3 ]] ; then
          sync=lock_all
      fi
      if [[ $j == 4 ]] ; then
          sync=flush
      fi
      if [[ $j == 5 ]] ; then
          sync=flush_local
      fi

      cp ./basic_${test_type}_${op}.sh ./basic_${test_type}_${op}_${sync}.sh
      sed -i "s/sync=/sync=${sync}/g" ./basic_${test_type}_${op}_${sync}.sh

      ## loop over #ops
      for ((nops=$min_nops; nops<=$max_nops; nops*=$interval_nops))
      do
        cp ./basic_${test_type}_${op}_${sync}.sh ./basic_${test_type}_${op}_${sync}_o${nops}.sh
        sed -i "s/nops=/nops=${nops}/g" ./basic_${test_type}_${op}_${sync}_o${nops}.sh

        ## loop over #procs
        for ((np=$min_np; np<=$max_np; np*=$interval_np))
        do
          cp ./basic_${test_type}_${op}_${sync}_o${nops}.sh ./run_${test_type}_${op}_${sync}_o${nops}_p${np}.sh
          sed -i "s/np=/np=${np}/g" ./run_${test_type}_${op}_${sync}_o${nops}_p${np}.sh
        done
      done
    done
  done

  ## delete intermediate files
  rm ./basic_${test_type}_*
done